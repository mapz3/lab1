#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

// Thread Synchronized  implementation of
// singleton design pattern

class Player
{ public:  int exp;
           int lvl;
           string login;
           string password;

    static Player *obj;
protected: Player(const std::string value): value_(value) {}
    ~Player() {}
    std::string value_;
    private: Player(){};
    static std::mutex mutex_;
// Only one thread can execute this at a time

            public:
    //Singletons should not be cloneable.
         Player(Player &other) = delete;

    //Singletons should not be assignable
        void operator=(const Player &) = delete;

        static Player *GetInstance(const std::string& value){

       // static Player *getInstance(){
                 if (obj == nullptr)
                  {  std::lock_guard<std::mutex> lock(mutex_);
                     obj = new Player();
                     cout<<value<<endl;}
                     return obj;

                  }

    int getexp() {
          return this -> exp;
       }

    void setexp(int exp) {
          this -> exp = exp;
       }

    int getlvl() {
          return this -> lvl;
       }

    void setlvl(int lvl) {
          this -> lvl = lvl;
       }
    string getlogin() {
          return this -> login;
       }

    void setlogin(string login) {
          this -> login = login;
       }
    string getpassword() {
          return this -> password;
       }

    void setpassword(string password) {
          this -> password = password;
       }
    std::string value() const{
           return value_;
       }
};

//Initialize pointer to zero so that it can be initialized in first call to getInstance
Player *Player::obj = 0;
std::mutex Player::mutex_;


void ThreadFoo(){
    // Following code emulates slow initialization.
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    Player* singleton = Player::GetInstance("1");
    std::cout << singleton->value() << "\n";
}

void ThreadBar(){
    // Following code emulates slow initialization.
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    Player* singleton = Player::GetInstance("2");
    std::cout << singleton->value() << "\n";
}
int main()
{
    std::cout <<"If you see the same value, then singleton was reused (yay!\n" <<
                   "If you see different values, then 2 singletons were created (booo!!)\n\n" <<
                   "RESULT:\n";
       std::thread t1(ThreadFoo);
       std::thread t2(ThreadBar);
       t1.join();
       t2.join();

       return 0;
}
