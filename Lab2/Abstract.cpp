#include <iostream>

using namespace std;


class Furniture {
 public:
  virtual ~Furniture(){};
  virtual std::string UsefulFunctionA() const = 0;
};

class Closet : public Furniture {
 public:
  std::string UsefulFunctionA() const override {
    return "You bought Closet.";
  }
};

class Bed : public Furniture {
  std::string UsefulFunctionA() const override {
    return "You bought Bed.";
  }
};

/**
 * Here's the the base interface of another product. All products can interact
 * with each other, but proper interaction is possible only between products of
 * the same concrete variant.
 */
class Technique {

 public:
  virtual ~Technique(){};
  virtual std::string UsefulFunctionB() const = 0;
  /**
   * ...but it also can collaborate with the ProductA.
   *
   * The Abstract Factory makes sure that all products it creates are of the
   * same variant and thus, compatible.
   */
  virtual std::string AnotherUsefulFunctionB(const Furniture &collaborator) const = 0;
};

/**
 * Concrete Products are created by corresponding Concrete Factories.
 */
class Refrigirator : public Technique {
 public:
  std::string UsefulFunctionB() const override {
    return "You bought refrigirator.";
  }
  /**
   * The variant, Product B1, is only able to work correctly with the variant,
   * Product A1. Nevertheless, it accepts any instance of AbstractProductA as an
   * argument.
   */
  std::string AnotherUsefulFunctionB(const Furniture &collaborator) const override {
    const std::string result = collaborator.UsefulFunctionA();
    return "You bought refrigerator with ( " + result + " )";
  }
};

class AirCon : public Technique {
 public:
  std::string UsefulFunctionB() const override {
    return "You bought Air-Conditioner.";
  }
  /**
   * The variant, Product B2, is only able to work correctly with the variant,
   * Product A2. Nevertheless, it accepts any instance of AbstractProductA as an
   * argument.
   */
  std::string AnotherUsefulFunctionB(const Furniture &collaborator) const override {
    const std::string result = collaborator.UsefulFunctionA();
    return "You bought Air-Conditioner with ( " + result + " )";
  }
};

/**
 * The Abstract Factory interface declares a set of methods that return
 * different abstract products. These products are called a family and are
 * related by a high-level theme or concept. Products of one family are usually
 * able to collaborate among themselves. A family of products may have several
 * variants, but the products of one variant are incompatible with products of
 * another.
 */
class AbstractFactory {
 public:
  virtual Furniture *CreateProductA() const = 0;
  virtual Technique *CreateProductB() const = 0;
};

/**
 * Concrete Factories produce a family of products that belong to a single
 * variant. The factory guarantees that resulting products are compatible. Note
 * that signatures of the Concrete Factory's methods return an abstract product,
 * while inside the method a concrete product is instantiated.
 */
class ConcreteFactory1 : public AbstractFactory {
 public:
  Furniture *CreateProductA() const override {
    return new Closet();
  }
  Technique *CreateProductB() const override {
    return new Refrigirator();
  }
};

/**
 * Each Concrete Factory has a corresponding product variant.
 */
class ConcreteFactory2 : public AbstractFactory {
 public:
  Furniture *CreateProductA() const override {
    return new Bed();
  }
  Technique *CreateProductB() const override {
    return new AirCon();
  }
};

/**
 * The client code works with factories and products only through abstract
 * types: AbstractFactory and AbstractProduct. This lets you pass any factory or
 * product subclass to the client code without breaking it.
 */

void ClientCode(const AbstractFactory &factory) {
  const Furniture *product_a = factory.CreateProductA();
  const Technique *product_b = factory.CreateProductB();
  std::cout << product_b->UsefulFunctionB() << "\n";
  std::cout << product_b->AnotherUsefulFunctionB(*product_a) << "\n";
  delete product_a;
  delete product_b;
}

int main() {
  std::cout << "Client: Testing client code with the first factory type:\n";
  ConcreteFactory1 *f1 = new ConcreteFactory1();
  ClientCode(*f1);
  delete f1;
  std::cout << std::endl;
  std::cout << "Client: Testing the same client code with the second factory type:\n";
  ConcreteFactory2 *f2 = new ConcreteFactory2();
  ClientCode(*f2);
  delete f2;
  return 0;
}
