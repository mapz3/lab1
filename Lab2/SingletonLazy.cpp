#include <iostream>
#include <string>

using namespace std;

class User{
    static User* instance;
    string password;
    string login;
    int money ;
    int level;
    int exp;

private: User() {
    password = "";
        login = "";
        money=10000;
        level = 1;
        exp = 1;
    }

private: User(string myPassword, string  myLogin, int myMoney, int  myLevel, int  myPower)
    {

        password = myPassword;
        login = myLogin;
        money=myMoney;
        level = myLevel;
        exp = myPower;
    }
    // all constructors should be private or public(iff you want to allow inheritance)

public:
    static User* getInstace() {
        if(instance == NULL)
            instance = new User();
        return instance;
    }
    int increaseLevel() {return level++;}
    int increaseExp(){return exp++;}
    bool isCorrectPassword() {
        if (password.length() < 6)
            return false;
        else {
            for (int i = 0; i < password.length(); i++) {
                if (isdigit(password[i])) {
                    return true;
                }
                else return false;
            }

        }
    }
     void Buy(){}
     bool isUniqueLogin(){return true;}
    int getLevel() {return level;}
    int getPower() {return exp;}
    string getLogin() {return login;}
    string getPassword(){return password;}
    int getMoney(){return money;}



    string changeLogin(string newLogin) {
     login = newLogin;
     if(this->isUniqueLogin())
         return "Login changed successfully";
     else return "Such login already exists. Please enter other login" ;
    }

    string changePassword(string newPassword) {
     password = newPassword;
     if(this->isCorrectPassword())
         return "Password changed successfully";
     else return "Password is too short. Please enter longer password";
    }

    void getUser() {
        cout << "Login: " << login << endl;
        cout << "Level: " << level << endl;
        cout << "Power: " << exp << endl << endl;
    }
};

User * User::instance = 0;

void someFunction () {
    User *myUser = myUser->getInstace();
    myUser->getUser();
}

int main() {

    User *myUser = myUser->getInstace();
    myUser->changeLogin("Player1");

    someFunction();

    system("pause");
    return 0;
}
