#include <iostream>
#include<string>
#include<fstream>

using namespace std;


class User;

class Memento
{
  public:
    Memento(string myName, int myType, int myExp, int myLevel)
    {
        name = myName;
        type = myType;
        exp = myExp;
        level = myLevel;
    }
  private:
    friend class  User;
    friend class  Player;
    friend class  Admin;
    string name;
      int type;
      float rooms;
      float reserved;
      int exp;
      int level;
};

class  User
{
  public:
      User(){}
      virtual void roomsQuantityDown() = 0;
      virtual void roomsQuantityUp() = 0;
      virtual void reservedUp() = 0;
      virtual void reservedDown() = 0;
      virtual void increaseExp() = 0;
      virtual void increaseLevel() = 0;
      virtual Memento *createMemento() = 0;
      virtual void ShowUser() = 0;
      virtual void reinstateMemento(Memento *mem) = 0;
  private:
    //int _value;


};

class Player: public User
{
private:
     string name;
      int type;
      float rooms;
      float reserved;
      int exp;
      int level;
public:
     Player(string myName, int myType, int myExp, int myLevel)
    {
        name = myName;
        type = myType;
        rooms = 0;
        reserved = 0;
        exp = myExp;
        level = myLevel;
    }
     void ShowUser()
     {
         cout << "Name: " << this->name << endl;
         cout << "Type: Player " << endl;
         cout << "Numbers of rooms you own: " << this->rooms << ",reserved: " << this->reserved << endl;
         cout << "Your exp = " << this->exp << endl;
         cout << "Your level = " << this->level << endl;
         ofstream out("Player.txt", std::ios::app);
        if (out.is_open())
        {
         out << "Name: " << this->name << endl;
         out << "Type: Player " << endl;
         out << "Numbers of rooms you own: "<< this->rooms << ",reserved: " << this->reserved << endl;
         out << "Your exp = " << this->exp << endl;
         out << "Your level = " << this->level << endl;
        }

     }
    void roomsQuantityDown()
    {
        this->rooms--;
        ShowUser();

    }
    void roomsQuantityUp()
    {
        this->rooms++;
        ShowUser();
    }
    void reservedUp()
    {
        this->reserved++;
        ShowUser();
    }
    void reservedDown()
    {
        this->reserved--;
        ShowUser();
    }


    void increaseExp()
    {
       this->exp++;
        ShowUser();

    }
    void increaseLevel()
    {
        this->level++;
        ShowUser();
    }

    Memento *createMemento()
    {
        return new Memento(name, type, exp, level);
    }
    void reinstateMemento(Memento *mem)
    {
        name = mem->name;
        type = mem->type;
        exp = mem->exp;
        level = mem->level;
    }

};
class Admin: public User
{
    private:
     string name;
      int type;
      float rooms;
      float reserved;
      int exp;
      int level;
public:
     Admin(string myName, int myType, int myExp, int myLevel)
    {
        name = myName;
        type = myType;
        rooms = 0;
        reserved = 0;
        exp = myExp;
        level = myLevel;
    }
     void ShowUser()
     {
         cout << "Name: " << this->name << endl;
         cout << "Type: Admin" << endl;
         cout << "Numbers of rooms to prepare: [" << this->rooms << ",reserved: " << this->reserved << "]" << endl;
         cout << "Your exp = " << this->exp << endl;
         cout << "Your hotels level = " << this->level << endl;
          ofstream out("admin.txt", std::ios::app);
        if (out.is_open())
        {
         out << "Name: " << this->name << endl;
         out << "Type: Admin " << endl;
         out << "Numbers of rooms to prepare: [" << this->rooms << ",reserved: " << this->reserved << "]" << endl;
         out << "Your exp = " << this->exp << endl;
         out << "Your hotels level = " << this->level << endl;
        }

     }
    void roomsQuantityDown()
    {
        this->rooms--;
        ShowUser();

    }
    void roomsQuantityUp()
    {
        this->rooms++;
        ShowUser();
    }
    void reservedUp()
    {
        this->reserved++;
        ShowUser();
    }
    void reservedDown()
    {
        this->reserved--;
        ShowUser();
    }

    void increaseExp()
    {
       this->exp++;
        ShowUser();

    }
    void increaseLevel()
    {
        this->level++;
        ShowUser();
    }

    Memento *createMemento()
    {
        return new Memento(name, type, exp, level);
    }
    void reinstateMemento(Memento *mem)
    {
        name = mem->name;
        type = mem->type;
        exp = mem->exp;
        level = mem->level;
    }
};
class Command
{
  public:
    typedef void( User:: *Action)();
    Command( User *receiver, Action action)
    {
        _receiver = receiver;
        _action = action;
    }
    virtual void execute()
    {
        _mementoList[_numCommands] = _receiver->createMemento();
        _commandList[_numCommands] = this;
        if (_numCommands > _highWater)
          _highWater = _numCommands;
        _numCommands++;
        (_receiver->*_action)();
    }
    static void undo()
    {
        if (_numCommands == 0)
        {
            cout << "*** Nothing to return!! ***" << endl;
            return ;
        }
           _commandList[_numCommands - 1]->_receiver->reinstateMemento
          (_mementoList[_numCommands - 1]);
        _numCommands--;
    }
    void static redo()
    {
        if (_numCommands > _highWater)
        {
            cout << "*** Can't return something that has'nt been undone!! ***" << endl;
            return ;
        }
  (_commandList[_numCommands]->_receiver->*(_commandList[_numCommands]
          ->_action))();
        _numCommands++;
    }
  protected:
     User *_receiver;
    Action _action;
    static Command *_commandList[20];
    static Memento *_mementoList[20];
    static int _numCommands;
    static int _highWater;
};

Command *Command::_commandList[];
Memento *Command::_mementoList[];
int Command::_numCommands = 0;
int Command::_highWater = 0;

int main()
{
  string myName; int type;
  cout << "Enter Name: ";
  cin >> myName;
  cout << "Choose: Player(press 1) or Admin(press 2): ";
  cin >> type;
  User *object;
  if(type == 1)
   object = new  Player(myName, type, 10, 1);
  else if(type == 2)
       object = new  Admin(myName, type, 10, 1);
 object->ShowUser();
  Command *commands[7];
  commands[1] = new Command(object, & User::roomsQuantityDown);
  commands[2] = new Command(object, & User::roomsQuantityUp);
  commands[3] = new Command(object, & User::reservedUp);
  commands[4] = new Command(object, & User::reservedDown);
  commands[5] = new Command(object, & User::increaseExp);
  commands[6] = new Command(object, & User::increaseLevel);


 int i;
  cout << "To Exit press 0\n"<<
          "To DecreaseRoomsQuantity press 1\n"<<
          "To IncreaseRoomsQuantity press 2\n"<<
          "To ReserveRoom press 3\n"<<
          "To CancelReservation press 4\n"<<
          "To IncreaseExp press 5\n"<<
          "To IncreaseLevel press 6\n"<<
          "To Undo press 7\n"<<
          "To Redo  press 8:";
  cin >> i;

  while (i)
  {
    if (i == 7)
      Command::undo();
    else if (i == 8)
      Command::redo();
    else
      commands[i]->execute();
    cout << "To Exit press 0\n"<<
            "To DecreaseRoomsQuantity press 1\n"<<
            "To IncreaseRoomsQuantity press 2\n"<<
            "To ReserveRoom press 3\n"<<
            "To CancelReservation press 4\n"<<
            "To IncreaseExp press 5\n"<<
            "To IncreaseLevel press 6\n"<<
            "To Undo press 7\n"<<
            "To Redo  press 8:";
    cin >> i;
  }
  system("pause");
  return 0;
}
