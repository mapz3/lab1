#include <iostream>
#include<string>
#include<fstream>
using namespace std;
class User;

class State
{
  public:
    float room;
    float reserved;
    int exp;
    int level;
    State(){
        room=15;
        reserved=5;
        exp=50;
        level=1;}

        virtual ~State(){}
        virtual void roomsQuantityDown() = 0;
        virtual void roomsQuantityUp() = 0;
        virtual void reservedUp() = 0;
        virtual void reservedDown() = 0;
        virtual void increaseExp() = 0;
        void increaseLevel(){this->level++;	ShowUser();}

        void ShowUser()
        {
            cout << "Player: " << endl;
            cout << "Numbers of rooms you own: [" << this->room << ",reserved: " << this->reserved << "]" << endl;
            cout << "Your exp = " << this->exp << endl;
            cout << "Your level = " << this->level << endl;}
};

    class FirstState : public State
    {
    private:
      public:
        void roomsQuantityUp()
        {
            this->room++;
            ShowUser();
        }
        void reservedUp()
        {
            this->reserved++;
            ShowUser();
        }

        void increaseExp()
        {
           this->exp++;
            ShowUser();

        }
        void roomsQuantityDown()
        {
            this->room--;
            ShowUser();
        }
        void reservedDown()
        {
            this->reserved--;
            ShowUser();
        }

    };
    class SecondState : public State
    {
        private:
      public:
        void reservedDown()
        {
            this->reserved--;
            ShowUser();
        }
        void roomsQuantityDown()
        {
            this->room--;
            ShowUser();
        }
        void reservedUp()
        {
            this->reserved++;
            ShowUser();
        }
        void increaseExp()
        {
           this->exp--;
            ShowUser();

        }

        void roomsQuantityUp()
        {
            this->room--;
            ShowUser();
        }

    };

    class ThirdState : public State
      {
    private:
    public:
        void roomsQuantityDown()
        {
            this->room--;
            ShowUser();

        }
        void reservedDown()
        {
            this->reserved--;
            ShowUser();
        }
        void increaseExp()
        {
           this->exp++;
            ShowUser();
        }
        void roomsQuantityUp()
        {
            this->room++;
            ShowUser();
        }

        void reservedUp()
        {
            this->reserved++;
            ShowUser();
        }

    };

 class User
    {
      public:
        User ( State* comp): a(comp) {	}
       ~User() { delete a; }
void roomsQuantityDown()
{
    a->roomsQuantityDown();

}
void roomsQuantityUp()
{
    a->roomsQuantityUp();

}
void reservedUp()
{
    a->reservedUp();

}
void reservedDown()
{
    a->reservedDown();

}

void increaseExp()
{
    a->increaseExp();


}
void increaseLevel()
{
    a->increaseLevel();

}

      private:
        State* a;
    };

 int main()
 {
     int answ;
     User* a;
     cout << "Please choose one scenario:\nFor First press 1, please \n For Second press 2, please \nFor Third press 3, please " <<endl;
     cin >> answ;
     switch(answ ){
     case 1:
       a = new User(new FirstState);
        break;
     case 2:
       a = new User(new SecondState);
        break;
     case 3:
       a = new User(new ThirdState);
        break;
     }
     cout << "To DecreaseRoomsQuantity press 1\n"<<
             "To IncreaseRoomsQuantity press 2\n"<<
             "To ReserveRoom press 3\n"<<
             "To CancelReservation press 4\n"<<
             "To IncreaseExp press 5\n"<<
             "To IncreaseLevel press 6\n";
     while(answ){
       cin >> answ;
      switch(answ ){
      case 1:
         a->roomsQuantityDown();
          break;
      case 2:
         a->roomsQuantityUp();
          break;
       case 3:
         a->reservedUp();
          break;
       case 4:
         a->reservedDown();
          break;
       case 5:
         a->increaseExp();
          break;
      case 6:
         a->increaseLevel();
        break;
      }
     }

   delete a;
   return 0;
 }



