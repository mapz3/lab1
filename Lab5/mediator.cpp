#include <iostream>
#include <string>

class BaseComponent;
class Mediator {
 public:
  virtual void Notify(BaseComponent *sender, std::string event) const = 0;
};


class BaseComponent {
 protected:
  Mediator *mediator_;
  int rooms = 10;
  int reserved = 5;
  int level = 1;
  int exp = 100;
 public:
  BaseComponent(Mediator *mediator = nullptr) : mediator_(mediator) {
  }
  void set_mediator(Mediator *mediator) {
    this->mediator_ = mediator;
  }
};


class Rooms : public BaseComponent {
 public:
  void RoomsUp() {
    std::cout << "Rooms quantity has been increased so is exp.\n";
    rooms++;
    exp+=10;
    std::cout<<"Rooms quantity now is "<<rooms<<" and exp="<<exp<<"\n";
    this->mediator_->Notify(this, "A");
  }
  void RoomsDown() {
    std::cout << "Rooms quantity has been decreased so is exp.\n";
    rooms--;
    exp-=10;
    std::cout<<"Rooms quantity now is "<<rooms<<" and exp="<<exp<<"\n";
    this->mediator_->Notify(this, "B");
  }
};

class Level : public BaseComponent {
 public:
  void LevelUp() {
      std::cout << "Level quantity has increased so is exp.\n";
      level++;
      exp+=10;
      std::cout<<"Level now is "<<level<<" and exp="<<exp<<"\n";
    this->mediator_->Notify(this, "C");
  }
  void LevelDovn() {
      std::cout << "Level quantity has decreased so is exp.\n";
      level--;
      exp-=10;
      std::cout<<"Level now is "<<level<<" and exp="<<exp<<"\n";
    this->mediator_->Notify(this, "D");
  }
};


class ConcreteMediator : public Mediator {
 private:
  Rooms *component1_;
  Level *component2_;

 public:
  ConcreteMediator(Rooms *c1, Level *c2) : component1_(c1), component2_(c2) {
    this->component1_->set_mediator(this);
    this->component2_->set_mediator(this);
  }
  void Notify(BaseComponent *sender, std::string event) const override {
    if (event == "A") {
      std::cout << "Mediator reacts on A and triggers following operations:\n";
      this->component2_->LevelUp();
    }
    if (event == "D") {
      std::cout << "Mediator reacts on D and triggers following operations:\n";
      this->component1_->RoomsDown();
      this->component2_->LevelUp();
    }
  }
};

void ClientCode() {
  Rooms *c1 = new Rooms;
  Level *c2 = new Level;
  ConcreteMediator *mediator = new ConcreteMediator(c1, c2);
  std::cout << "Client triggers operation A.\n";
  c1->RoomsUp();
  std::cout << "\n";
  std::cout << "Client triggers operation D.\n";
  c2->LevelDovn();

  delete c1;
  delete c2;
  delete mediator;
}

int main() {
  ClientCode();
  return 0;
}
