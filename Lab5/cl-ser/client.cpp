#include <windows.h>
#include <conio.h>
#include <stdio.h>
#include <iostream>
#include<string>
using namespace std;
 


DWORD main(int argc, char *argv[])
{
  
  HANDLE hNamedPipe;

 
  DWORD  cbWritten;

  
  DWORD  cbRead;

  
  char   szBuf1[256];
  char   szBuf2[512];

  
  char   szPipeName[256];

  //printf("Named pipe client demo\n"
   // "(C) A. Frolov, 1996, Email: frolov@glas.apc.org\n\n");

 // printf("Syntax: pipec [servername]\n");

  
  if(argc > 1)
    sprintf(szPipeName, "\\\\%s\\pipe\\$MyPipe$",
      argv[1]);
  
 
  else
    strcpy(szPipeName, "\\\\.\\pipe\\$MyPipe$");

  
  hNamedPipe = CreateFile(
    szPipeName, GENERIC_READ | GENERIC_WRITE,
    0, NULL, OPEN_EXISTING, 0, NULL);
    
  
  if(hNamedPipe == INVALID_HANDLE_VALUE)
  {
    fprintf(stdout,"CreateFile: Error %ld\n", 
      GetLastError());
    getch();
    return 0;
  }

  
  fprintf(stdout,"\nConnected. Type 'exit' to terminate\n"); 

  
  while(1)
  {
    
   // printf("cmd>");
	 
    
    gets_s(szBuf1);

   
    if(!WriteFile(hNamedPipe, szBuf1, strlen(szBuf1) + 1,
      &cbWritten, NULL))
      break;
    
    
    if(ReadFile(hNamedPipe, szBuf2, 512, &cbRead, NULL))
      printf("Server: <%s>\n", szBuf2);
    
    
    else
    {
      fprintf(stdout,"ReadFile: Error %ld\n", 
        GetLastError());
      getch();
      break;
    }
    
    
    if(!strcmp(szBuf1, "exit") || !strcmp(szBuf2, "exit") )
      break;
  }

  
  CloseHandle(hNamedPipe);
  return 0;
}