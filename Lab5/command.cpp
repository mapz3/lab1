#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;



class Player
{
private:
     string name;
      float rooms;
      float reserved;
      int exp;
      int level;
public:
     Player(){
        rooms = 10;
        reserved = 5;
        exp = 100;
        level = 1;
    }
     void create(){
         cout << "Player is created" << endl;
       cout << "Numbers of rooms you own: " <<rooms << ",reserved: " << reserved<< "Your exp = " << exp <<"Your level = " <<level << endl;
     };
     void ShowUser()
     {

         cout << "Numbers of rooms you own: " << this->rooms << ",reserved: " << this->reserved << endl;
         cout << "Your exp = " << this->exp << endl;
         cout << " Your level = " << this->level << endl;
         ofstream out("Player.txt", std::ios::app);
        if (out.is_open())
        {
         out << " Numbers of rooms you own: " << this->rooms << " ,reserved: " << this->reserved<<" " << endl;
         out << " Your exp = " << this->exp<<" " << endl;
         out << " Your level = " << this->level << endl;
        }

     }
     void save( string file ) {
         cout << "Save game in " << file << endl;
          ofstream out(file, std::ios::app);
           if (out.is_open())
           {
               out << " Numbers of rooms you own: " << this->rooms << " ,reserved: " << this->reserved<<" " << endl;
               out << " Your exp = " << this->exp<<" " << endl;
               out << " Your level = " << this->level << endl;
     }}
    void roomsQuantityDown()
    {
        this->rooms--;
        ShowUser();

    }
    void roomsQuantityUp()
    {
        this->rooms++;
        ShowUser();
    }
    void reservedUp()
    {
        this->reserved++;
        ShowUser();
    }
    void reservedDown()
    {
        this->reserved--;
        ShowUser();
    }


    void increaseExp()
    {
       this->exp++;
        ShowUser();

    }
    void increaseLevel()
    {
        this->level++;
        ShowUser();
    }
};

/**
 * The Command interface declares a method for executing a command.
 */
class Command {
private:
protected:
   Command(Player* p ): player( p) {}
   Player * player;

 public:
  virtual ~Command() {}
  virtual void execute() = 0;

};
/**
 * Some commands can implement simple operations on their own.
 */
class Start : public Command {
 public:
   Start(Player* p ) : Command( p) {}
  void execute() {
    player->create( );}
  };

/**
 * However, some commands can delegate more complex operations to other objects,
 * called "receivers."
 */
class Save : public Command {

public:
  Save( Player* p ) : Command(p) {}
  void execute( ) {
    string file_name;
    cout<<"Enter file name please\n";
    cin>>file_name;
    player->save( file_name);
  }

};

class ChooseAction : public Command
{
public:
    ChooseAction( Player* p) : Command( p) {}
    void execute() {
      player->save( "game.txt");
      int act;
      cout << "To DecreaseRoomsQuantity press 1\n"<<
              "To IncreaseRoomsQuantity press 2\n"<<
              "To ReserveRoom press 3\n"<<
              "To CancelReservation press 4\n"<<
              "To IncreaseExp press 5\n"<<
              "To IncreaseLevel press 6\n";
      cin >> act;
      switch(act ){
      case 1:
         player->roomsQuantityDown();
          break;
      case 2:
         player->roomsQuantityUp();
          break;
       case 3:
         player->reservedUp();
          break;
       case 4:
         player->reservedDown();
          break;
       case 5:
         player->increaseExp();
          break;
      case 6:
         player->increaseLevel();
          break;
                   }
    }
};

int main() {
    Player player;

      vector<Command*> v;

      v.push_back( new Start(&player));
      v.push_back( new ChooseAction( &player));
      v.push_back( new Save( &player));
      v[0]->execute();
      int answ = 1;
      while(answ){

         if(answ == 1){
//             cout << "To DecreaseRoomsQuantity press 1\n"<<
//                     "To IncreaseRoomsQuantity press 2\n"<<
//                     "To ReserveRoom press 3\n"<<
//                     "To CancelReservation press 4\n"<<
//                     "To IncreaseExp press 5\n"<<
//                     "To IncreaseLevel press 6\n";
           v[1]->execute();
         }
         else if(answ == 2)
           v[2]->execute();
         cout << "To Choose Action press 1, To Save press 2\n";
         cin >> answ;
     }

      for (size_t i=0; i<v.size(); ++i)
        delete v[i];

        system("pause");
        return 0;

  return 0;
}
